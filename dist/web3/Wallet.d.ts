import EthereumAddress from "./EthereumAddress";
export default class Wallet extends EthereumAddress {
    private __walletName;
    constructor(walletAddr: string, ownerName?: string);
    toString: () => string;
    valueOf: () => string;
}
