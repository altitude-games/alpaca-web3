import EthereumAddress from "../EthereumAddress";
export default interface IMintableERC20 {
    mint: (recipient: EthereumAddress, amount: number) => Promise<void>;
}
