import { WebsocketProviderOptions, HttpProviderOptions } from 'web3-core-helpers';
export declare const HTTP_PROVIDER = "http";
export declare const WS_PROVIDER = "ws";
export declare const IPC_PROVIDER = "ipc";
export declare const HD_WALLET_PROVIDER = "hdwallet";
export declare const HD_WALLET_PROVIDER_WS = "wsHDwallet";
export default interface ChainNetConfig {
    walletOptions: {
        mnemonic?: {
            phrase: string;
        };
        privateKeys: Array<string>;
        addressIndex: number;
        numberOfAddresses: number;
        pollingInterval?: number;
        derivationPath?: string;
    };
    httpUrl: Array<string>;
    wsUrl?: Array<string>;
    ipcUrl?: string;
    httpOptions?: HttpProviderOptions;
    wsOptions?: WebsocketProviderOptions;
    useProvider: 'http' | 'ws' | 'ipc' | 'hdwallet' | 'wsHDwallet';
    networkId: number;
    networkBlockSteps?: number;
    options: {
        gas: number;
        gasPrice: string;
    };
}
