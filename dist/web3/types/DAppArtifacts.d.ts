import Web3 from 'web3';
import { Contract } from 'web3-eth-contract';
import { ChainNetConfig } from '.';
import { EthereumAddress } from '..';
import { ChainConfigOverride } from '../utils';
export default interface DAppArtifacts {
    networkConfig: ChainNetConfig;
    contractName: string;
    contract: Contract;
    web3: Web3;
    addresses: Array<EthereumAddress>;
    overrides?: ChainConfigOverride;
}
