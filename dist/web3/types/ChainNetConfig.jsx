"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HD_WALLET_PROVIDER_WS = exports.HD_WALLET_PROVIDER = exports.IPC_PROVIDER = exports.WS_PROVIDER = exports.HTTP_PROVIDER = void 0;
exports.HTTP_PROVIDER = "http";
exports.WS_PROVIDER = "ws";
exports.IPC_PROVIDER = "ipc";
exports.HD_WALLET_PROVIDER = "hdwallet";
exports.HD_WALLET_PROVIDER_WS = "wsHDwallet";
