"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Address_1 = __importDefault(require("./Address"));
class EthereumAddress {
    constructor(walletAddr) {
        const wallet = Address_1.default(walletAddr);
        if (!wallet) {
            throw new Error(`Failed initiating wallet address -- ${walletAddr}`);
        }
        this._wallet = wallet;
    }
    toString() { return this._wallet; }
    valueOf() { return this._wallet; }
    get value() {
        return this.valueOf();
    }
    get $() {
        return this.valueOf();
    }
    is(compareTo) {
        var _a;
        if (typeof compareTo === 'string') {
            return ((_a = this._wallet) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === compareTo.toLowerCase();
        }
        else {
            return this._wallet === compareTo.valueOf();
        }
    }
    equals(compareTo) {
        return this.is(compareTo);
    }
    ;
}
exports.default = EthereumAddress;
