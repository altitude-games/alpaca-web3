"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(Base) {
    return class BurnableERC20 extends Base {
        async burn(account, amount) {
            await this.contract.methods.mint(account.$, amount).send();
        }
    };
}
exports.default = default_1;
