"use strict";
// created from 'create-ts-index'
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MintableERC20 = exports.BurnableERC20 = void 0;
var BurnableERC20_1 = require("./BurnableERC20");
Object.defineProperty(exports, "BurnableERC20", { enumerable: true, get: function () { return __importDefault(BurnableERC20_1).default; } });
var MintableERC20_1 = require("./MintableERC20");
Object.defineProperty(exports, "MintableERC20", { enumerable: true, get: function () { return __importDefault(MintableERC20_1).default; } });
