"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(Base) {
    return class MintableERC20 extends Base {
        async mint(recipient, amount) {
            await this.contract.methods.mint(recipient.$, amount).send();
        }
    };
}
exports.default = default_1;
