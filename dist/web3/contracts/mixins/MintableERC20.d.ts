/// <reference types="bunyan" />
import ERC20 from "../ERC20";
import EthereumAddress from "../../EthereumAddress";
declare type GConstructor<T = {}> = new (...args: any[]) => T;
declare type ERC20Type = GConstructor<ERC20>;
export default function <TBase extends ERC20Type>(Base: TBase): {
    new (...args: any[]): {
        mint(recipient: EthereumAddress, amount: number): Promise<void>;
        decimals(): Promise<any>;
        balanceOf(wallet: import("../..").Wallet): Promise<number>;
        transfer(recipient: EthereumAddress, amount: number, callbacks?: {
            [event: string]: Function;
        } | undefined): Promise<any>;
        allowance(owner: EthereumAddress, spender: EthereumAddress): Promise<number>;
        approve(spender: EthereumAddress, amount: number, callbacks?: {
            [event: string]: Function;
        } | undefined): Promise<any>;
        transferFrom(sender: EthereumAddress, recipient: EthereumAddress, amount: number, callbacks?: {
            [event: string]: Function;
        } | undefined): Promise<any>;
        increaseAllowance(spender: EthereumAddress, addedValue: number, callbacks?: {
            [event: string]: Function;
        } | undefined): Promise<any>;
        decreaseAllowance(spender: EthereumAddress, removedValue: number, callbacks?: {
            [event: string]: Function;
        } | undefined): Promise<any>;
        _artifacts: import("../..").DAppArtifacts;
        _logger: import("bunyan");
        _cache: any;
        tokenName(): Promise<any>;
        tokenSymbol(): Promise<any>;
        readonly address: typeof EthereumAddress;
        readonly networkConfig: import("../..").ChainNetConfig;
        readonly contractName: string;
        readonly contract: import("web3-eth-contract").Contract;
        readonly web3: import("web3").default;
        readonly provider: any;
        readonly addresses: EthereumAddress[];
        readonly migrationHash: string;
        networkOverrides: import("../..").ChainConfigOverride | undefined;
        getPastEvents(eventName: string, startBlock?: number | undefined, endBlock?: number | undefined, filter?: import("web3-eth-contract").Filter | undefined): Promise<{
            events: import("web3-eth-contract").EventData[];
            fromBlock: number;
            toBlock: number;
        }>;
        __initializeWallet(): Promise<void>;
        accounts: EthereumAddress[];
        getPrimaryAccount(): Promise<EthereumAddress>;
        _currentGasPrice(): Promise<string>;
        _performContractSend(contractMethodGetter: (contract: import("web3-eth-contract").Contract) => any, callbacks?: {
            [events: string]: Function;
        } | undefined, functionDesc?: string | undefined): Promise<any>;
        _performContractCall<T>(contractMethodGetter: (contract: import("web3-eth-contract").Contract) => any, functionDesc?: string | undefined): Promise<T | null>;
        cleanUp(): void;
    };
} & TBase;
export {};
