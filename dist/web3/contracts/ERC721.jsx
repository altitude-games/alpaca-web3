"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EthereumAddress_1 = __importDefault(require("../EthereumAddress"));
const ContractBase_1 = __importDefault(require("./ContractBase"));
class ERC721 extends ContractBase_1.default {
    constructor(artifacts) {
        super(artifacts);
    }
    async baseUri() {
        if (!this._cache.baseUri) {
            this._cache.baseUri = await this._performContractCall((contract) => {
                return contract.methods.baseUri();
            }, `getting ${this.contractName} base URI`);
        }
        return this._cache.baseUri;
    }
    async tokenURI(tokenId) {
        return String(await this._performContractCall((contract) => {
            return contract.methods.tokenURI(tokenId);
        }, `getting ${this.contractName} token URI for ${tokenId}`));
    }
    async balanceOf(wallet) {
        return Number(await this._performContractCall((contract) => {
            return contract.methods.balanceOf(wallet.$);
        }, `getting ${this.contractName} balance of ${wallet.$}`));
    }
    async ownerOf(tokenId) {
        return new EthereumAddress_1.default(String(await this._performContractCall(async (contract) => {
            return await contract.methods.ownerOf(tokenId);
        }, `getting owner of ${this.contractName} ${tokenId}`)));
    }
    async tokenOfOwnerByIndex(owner, index) {
        return String(await this._performContractCall((contract) => {
            return contract.methods.tokenOfOwnerByIndex(owner.$, index);
        }, `getting token of ${owner.$} at index ${index}`));
    }
    async totalSupply() {
        return Number(await this._performContractCall((contract) => {
            return contract.methods.totalSupply();
        }, `getting ${this.contractName} total supply`));
    }
    async tokenByIndex(index) {
        return Number(await this._performContractCall((contract) => {
            return contract.methods.tokenByIndex(index);
        }, `getting ${this.contractName} token by index ${index}`));
    }
    async approve(spender, tokenId, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.approve(spender.$, tokenId);
        }, callbacks, `${await this.getPrimaryAccount()} approves ${spender.$} for ${this.contractName} ${tokenId}`);
    }
    async getApproved(tokenId) {
        return new EthereumAddress_1.default(String(await this._performContractCall((contract) => {
            return contract.methods.getApproved(tokenId);
        }, `getting whose approved for token ${this.contractName} ${tokenId}`)));
    }
    async setApprovalForAll(operator, approved, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.setApprovalForAll(operator.$, approved);
        }, callbacks, `${await this.getPrimaryAccount()} set approval to all of ${this.contractName} to ${operator.$} into ${approved}`);
    }
    async isApprovedForAll(owner, operator) {
        return Boolean(await this._performContractCall((contract) => {
            return contract.methods.isApprovedForAll(owner.$, operator.$);
        }, `checking if ${operator.$} is approved for ${owner.$}'s assets`));
    }
    async transferFrom(from, to, tokenId, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.transferFrom(from.$, to.$, tokenId);
        }, callbacks, `${await this.getPrimaryAccount()} transfers ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`);
    }
    async safeTransferFrom(from, to, tokenId, data, callbacks) {
        const msg = `${await this.getPrimaryAccount()} safe transfers ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`;
        if (data) {
            return this._performContractSend((contract) => {
                return contract.methods.safeTransferFrom(from.$, to.$, tokenId, data);
            }, callbacks, msg);
        }
        else {
            return this._performContractSend((contract) => {
                return contract.methods.safeTransferFrom(from.$, to.$, tokenId);
            }, callbacks, msg);
        }
    }
}
exports.default = ERC721;
