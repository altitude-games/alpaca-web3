"use strict";
// created from 'create-ts-index'
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERC721 = exports.ERC20 = exports.ContractBase = void 0;
__exportStar(require("./battle-cars"), exports);
__exportStar(require("./mixins"), exports);
var ContractBase_1 = require("./ContractBase");
Object.defineProperty(exports, "ContractBase", { enumerable: true, get: function () { return __importDefault(ContractBase_1).default; } });
var ERC20_1 = require("./ERC20");
Object.defineProperty(exports, "ERC20", { enumerable: true, get: function () { return __importDefault(ERC20_1).default; } });
var ERC721_1 = require("./ERC721");
Object.defineProperty(exports, "ERC721", { enumerable: true, get: function () { return __importDefault(ERC721_1).default; } });
