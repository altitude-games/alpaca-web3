/// <reference types="node" />
import EthereumAddress from '../EthereumAddress';
import DAppArtifacts from '../types/DAppArtifacts';
import IERC721 from '../types/IERC721';
import TokenId from '../types/TokenId';
import ContractBase from './ContractBase';
export default class ERC721 extends ContractBase implements IERC721 {
    constructor(artifacts: DAppArtifacts);
    baseUri(): Promise<any>;
    tokenURI(tokenId: TokenId): Promise<string>;
    balanceOf(wallet: EthereumAddress): Promise<number>;
    ownerOf(tokenId: TokenId): Promise<EthereumAddress>;
    tokenOfOwnerByIndex(owner: EthereumAddress, index: number): Promise<string>;
    totalSupply(): Promise<number>;
    tokenByIndex(index: number): Promise<number>;
    approve(spender: EthereumAddress, tokenId: TokenId, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    getApproved(tokenId: TokenId): Promise<EthereumAddress>;
    setApprovalForAll(operator: EthereumAddress, approved: boolean, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    isApprovedForAll(owner: EthereumAddress, operator: EthereumAddress): Promise<boolean>;
    transferFrom(from: EthereumAddress, to: EthereumAddress, tokenId: TokenId, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    safeTransferFrom(from: EthereumAddress, to: EthereumAddress, tokenId: TokenId, data?: Buffer, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
}
