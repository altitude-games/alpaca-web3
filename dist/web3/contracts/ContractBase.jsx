"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = __importDefault(require("web3"));
const bunyan_1 = __importDefault(require("bunyan"));
const hdwallet_provider_1 = __importDefault(require("@truffle/hdwallet-provider"));
const MnemonicConverter_1 = __importDefault(require("../utils/MnemonicConverter"));
const PrivateKeyConverter_1 = __importDefault(require("../utils/PrivateKeyConverter"));
const utils_1 = require("../utils");
const __1 = require("..");
const exceptions_1 = require("../exceptions");
const Web3CallWrapper_1 = __importDefault(require("../utils/Web3CallWrapper"));
class ContractBase {
    constructor(artifacts) {
        this._cache = {};
        this._artifacts = artifacts;
        this._logger = bunyan_1.default.createLogger({
            name: `Contract#${artifacts.contractName}`,
            level: process.env.NODE_ENV === 'development' ? bunyan_1.default.DEBUG : bunyan_1.default.INFO
        });
        this.__initializeWallet().then(() => {
            this._logger.debug(`${this.contractName} Wallet accounts initialized`, this._artifacts.addresses);
        });
    }
    async tokenName() {
        if (!this._cache.name) {
            this._cache.name = await this._performContractCall(async (contract) => {
                return await contract.methods.symbol();
            });
        }
        return this._cache.name;
    }
    async tokenSymbol() {
        if (!this._cache.symbol) {
            this._cache.symbol = await this._performContractCall(async (contract) => {
                return await contract.methods.symbol();
            });
        }
        return this._cache.symbol;
    }
    get address() {
        if (!this._cache.address) {
            this._cache.address = new __1.EthereumAddress(this.contract.options.address);
        }
        return this._cache.address;
    }
    get networkConfig() {
        return this._artifacts.networkConfig;
    }
    get contractName() {
        return this._artifacts.contractName;
    }
    get contract() {
        var _a;
        return (_a = this._artifacts) === null || _a === void 0 ? void 0 : _a.contract;
    }
    get web3() {
        return this._artifacts.web3;
    }
    get provider() {
        return this._artifacts.web3.currentProvider;
    }
    get addresses() {
        return this._artifacts.addresses;
    }
    get migrationHash() {
        if (!this._cache.migrationHash) {
            const ContractJSON = utils_1.loadContractJsonAbi(this._artifacts.contractName, this._artifacts.networkConfig.networkId);
            this._cache.migrationHash = ContractJSON.networks[this._artifacts.networkConfig.networkId].transactionHash;
        }
        return this._cache.migrationHash;
    }
    get networkOverrides() {
        return this._artifacts.overrides;
    }
    set networkOverrides(overrides) {
        this._artifacts.overrides = overrides;
    }
    /**
     * Retrieves events from past blocks
     *
     * @param eventName name of events e.g. Transfer
     * @param startBlock optional, if not specified will auto detect starting from migration block of contract and succeeding calls will just increment from last block obtained
     * @param endBlock optional, will be detected based on latest block or increment of ChainNetConfig.networkBlockSteps - whichever is lower
     * @param filter optional filter as per web3 documentation
     */
    async getPastEvents(eventName, startBlock, endBlock, filter) {
        this._logger.debug(`${this.contractName}.getPastEvents`, startBlock, endBlock, filter);
        let fromBlock, toBlock = 0;
        if (!startBlock) {
            this._logger.debug(`${this.contractName}.getPastEvents`, `detecting start block`);
            if (this._cache.getPastEventsToBlock) {
                this._logger.debug(`${this.contractName}.getPastEvents`, `start block found from cache`);
                fromBlock = this._cache.getPastEventsToBlock + 1;
            }
            else {
                this._logger.debug(`${this.contractName}.getPastEvents`, `start block obtain from network`);
                fromBlock = Number(await Web3CallWrapper_1.default(async (web3) => {
                    return (await web3.eth.getTransactionReceipt(this.migrationHash)).blockNumber;
                }, this.networkConfig));
            }
        }
        else {
            fromBlock = startBlock;
        }
        if (!endBlock) {
            this._logger.debug(`${this.contractName}.getPastEvents`, `detecting end block`);
            //if none specified, auto detect it based on the value of start block
            toBlock = Number(await Web3CallWrapper_1.default(async (web3) => {
                return await web3.eth.getBlockNumber();
            }, this.networkConfig));
            const blockSteps = Number(this._artifacts.networkConfig.networkBlockSteps || 10000);
            if (toBlock - fromBlock > blockSteps) {
                toBlock = fromBlock + blockSteps;
            }
        }
        else {
            toBlock = endBlock;
        }
        this._cache.getPastEventsFromBlock = fromBlock;
        this._cache.getPastEventsToBlock = toBlock;
        this._logger.debug(`${this.contractName}.getPastEvents`, startBlock, endBlock, filter);
        const events = await this.contract.getPastEvents(eventName, {
            fromBlock,
            toBlock,
            filter
        });
        return {
            events,
            fromBlock,
            toBlock
        };
    }
    async __initializeWallet() {
        const { networkConfig, addresses } = this._artifacts;
        if (Object.keys(addresses).length > 0)
            return;
        const generatedAddresses = !!networkConfig.walletOptions.mnemonic ? await MnemonicConverter_1.default(networkConfig.walletOptions.mnemonic.phrase, networkConfig.walletOptions.addressIndex, networkConfig.walletOptions.numberOfAddresses) :
            await PrivateKeyConverter_1.default(networkConfig.walletOptions.privateKeys, networkConfig.walletOptions.addressIndex, networkConfig.walletOptions.numberOfAddresses);
        generatedAddresses.forEach((address, index) => {
            this._artifacts.addresses[index] = new __1.EthereumAddress(address);
        });
    }
    get accounts() {
        return this.addresses;
    }
    set accounts(accounts) {
        if (this.addresses.length === 0) {
            this._artifacts.addresses = accounts;
        }
    }
    async getPrimaryAccount() {
        if (Object.keys(this.addresses).length === 0) {
            await this.__initializeWallet();
        }
        return this.addresses[0];
    }
    async _currentGasPrice() {
        const gasPrice = String(await Web3CallWrapper_1.default(async (web3) => {
            return await web3.eth.getGasPrice();
        }, this.networkConfig));
        return gasPrice;
    }
    /**
     * Send transaction wrapper with pre-emptive checks for gas expenditure and gas price to avoid wasted calls and losing ethereum.
     * Adding the functionality of auto retry via endpoint rotations.
     *
     * @param contractMethodGetter a function that accepts contract object to return the method of the contract to send
     * @param functionDesc optional message to describe this call
     */
    async _performContractSend(contractMethodGetter, callbacks, functionDesc) {
        var _a, _b;
        if (functionDesc) {
            this._logger.info(`[performContractSend] ${functionDesc}`);
        }
        const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);
        let web3CallResult = null;
        await this.__initializeWallet();
        let throwError = null;
        for (let i = 0; i < retryAttempts; i++) {
            try {
                //close current provider connections as necessary
                this.cleanUp();
                //open new set of artifacts
                const contractAtifacts = utils_1.getContract(this.contractName, this.networkConfig, this._artifacts.overrides);
                this._artifacts = { ...this._artifacts, ...contractAtifacts };
                const contractMethod = contractMethodGetter(contractAtifacts.contract);
                const contractOptions = { ...this.networkConfig.options, ...(_a = this.networkOverrides) === null || _a === void 0 ? void 0 : _a.options };
                if (!contractOptions.from) {
                    contractOptions.from = (await this.getPrimaryAccount()).$;
                }
                this._logger.debug(`Contract Send Options`, this.networkConfig.options);
                this._logger.debug(`Contract Send Override Options`, (_b = this.networkOverrides) === null || _b === void 0 ? void 0 : _b.options);
                this._logger.debug(`Contract Sender Option From`, contractOptions.from);
                this._logger.debug(`Contract Sender Option Gas`, contractOptions.gas);
                //gas allowance check
                if (!!contractOptions.gas) {
                    let gasAllowanceRequired = this.networkConfig.options.gas;
                    try {
                        //apparently, this call throws error if gas supplied is not enough
                        //so we will assume that gas supplied is not enough if this throws
                        gasAllowanceRequired = Number(await contractMethod.estimateGas(contractOptions));
                    }
                    catch (estimateError) {
                        //the error can also happen by other means like contract call exceptions 
                        //we will isoltae that case and let outer call decide on what to do
                        if (estimateError.toString().toLowerCase().indexOf("gas required exceeds allowance") < 0) {
                            throw estimateError;
                        }
                    }
                    this._logger.debug("Gas allowance estimate", gasAllowanceRequired);
                    if (gasAllowanceRequired >= contractOptions.gas) {
                        throw new exceptions_1.InsufficientGasError(contractOptions.gas, gasAllowanceRequired);
                    }
                }
                //somehow contractOptions.gasPrice turns into hex encoded string 
                //after calling estimateGas using this option, revert the value to string
                if (contractOptions.gasPrice.startsWith("0x")) {
                    contractOptions.gasPrice = parseInt(contractOptions.gasPrice, 16).toString();
                }
                this._logger.debug(`Contract Sender Option GasPrice`, contractOptions.gasPrice);
                //gas price check
                if (contractOptions.gasPrice !== undefined) {
                    const currentGasPrice = await (await this._currentGasPrice()).toString();
                    //this._logger.debug(`Gas!!`, typeof currentGasPrice, typeof contractOptions.gasPrice);
                    //this._logger.debug(`Gas!!`, currentGasPrice, contractOptions.gasPrice.toString());
                    if (currentGasPrice > contractOptions.gasPrice) {
                        throw new exceptions_1.LowGasPriceError(contractOptions.gasPrice, currentGasPrice);
                    }
                    else {
                        //set gas price as network's
                        contractOptions.gasPrice = currentGasPrice;
                    }
                }
                let sendCmd = contractMethod.send(contractOptions);
                if (callbacks) {
                    Object.keys(callbacks).forEach(event => {
                        if (event !== 'then') {
                            sendCmd = sendCmd.on(event, callbacks[event]);
                        }
                    });
                    if (callbacks['then']) {
                        sendCmd = sendCmd.then(callbacks['then']);
                        throwError = null; //clear error
                    }
                }
                sendCmd = sendCmd.catch((err) => {
                    this._logger.error(`_performContractSend caught -- `, err);
                    throwError = err;
                });
                web3CallResult = await sendCmd;
                break; //break if finished without errors
            }
            catch (err) {
                this._logger.error(`${(!!functionDesc ? functionDesc : "_performContractCall")} [error] `, err);
                throwError = err;
                if (err instanceof exceptions_1.InsufficientGasError || err instanceof exceptions_1.LowGasPriceError) {
                    break;
                }
            }
            finally {
                //
            }
        }
        if (throwError) {
            this._logger.debug('Re-Throw', throwError);
            return Promise.reject(throwError);
            //throw throwError;
        }
        ;
        /* Promise.resolve(web3CallResult); */
        return web3CallResult;
    }
    /**
     * Call wrapper that provides auto retry with endpoint rotations.
     *
     * @param contractMethodGetter a function that accepts contract object to return the method of the contract to call
     * @param functionDesc optional message to describe this call
     */
    async _performContractCall(contractMethodGetter, functionDesc) {
        if (functionDesc) {
            this._logger.info(`[performContractCall] ${functionDesc}`);
        }
        const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);
        let web3CallResult = null;
        await this.__initializeWallet();
        for (let i = 0; i < retryAttempts; i++) {
            try {
                //close current provider connections as necessary
                this.cleanUp();
                //open new set of artifacts
                const contractAtifacts = utils_1.getContract(this.contractName, this.networkConfig, { options: { from: (await this.getPrimaryAccount()).$ } });
                this._artifacts = { ...this._artifacts, ...contractAtifacts };
                const methodCall = await contractMethodGetter(contractAtifacts.contract);
                //this._logger.debug(`method getter`, contractMethodGetter);
                //this._logger.debug(`method getter call `, methodCall.call);
                web3CallResult = await methodCall.call()
                    .then(async (result) => {
                    return result;
                })
                    .catch(async (err) => {
                    return null;
                });
                break; //break if finished without errors
            }
            catch (err) {
                this._logger.error(`${(!!functionDesc ? functionDesc : "_performContractCall")} [error] -- `, err);
            }
        }
        return web3CallResult;
    }
    cleanUp() {
        /* this._logger.debug(`Clean up provider`, this.provider);
        this._logger.debug(`Typeof`, typeof this.provider); */
        if (this.provider instanceof hdwallet_provider_1.default) {
            this.provider.engine.stop();
        }
        if (this.provider instanceof web3_1.default.providers.WebsocketProvider) {
            this.provider.disconnect(0, 'clean up');
        }
        if (this.provider instanceof web3_1.default.providers.IpcProvider) {
            this.provider.reset();
        }
    }
}
exports.default = ContractBase;
