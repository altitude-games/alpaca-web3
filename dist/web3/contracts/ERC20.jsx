"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ContractBase_1 = __importDefault(require("./ContractBase"));
class ERC20 extends ContractBase_1.default {
    constructor(artifacts) {
        super(artifacts);
    }
    async decimals() {
        if (!this._cache.decimals) {
            this._cache.decimals = this._performContractCall((contract) => {
                return contract.methods.decimals();
            }, `getting ${this.contractName} decimals`);
        }
        return this._cache.decimals;
    }
    async balanceOf(wallet) {
        return Number(await this._performContractCall((contract) => {
            return contract.methods.balanceOf(wallet.$);
        }, `getting ${this.contractName} balance of ${wallet.$}`) || 0);
    }
    async transfer(recipient, amount, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.transfer(recipient.$, amount);
        }, callbacks, `${await this.getPrimaryAccount()} transferring ${amount} ${this.contractName} to ${recipient.$}`);
    }
    async allowance(owner, spender) {
        return Number(await this._performContractCall((contract) => {
            return contract.methods.allowance(owner.$, spender.$);
        }, `getting ${owner.$} ${this.contractName} allowance for ${spender.$}`) || 0);
    }
    async approve(spender, amount, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.approve(spender.$, amount);
        }, callbacks, `${await this.getPrimaryAccount()} approving ${spender.$} for ${amount} ${this.contractName}`);
    }
    async mint(recipient, amount, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.mint(recipient.$, amount);
        }, callbacks, `minting ${amount} ${this.contractName} tokens to ${recipient.$}`);
    }
    async transferFrom(sender, recipient, amount, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.transferFrom(sender.$, recipient.$, amount);
        }, callbacks, `transfer ${this.contractName} ${this.tokenSymbol} from ${sender.$} to ${recipient.$} with the amount of ${amount}`);
    }
    async increaseAllowance(spender, addedValue, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.increaseAllowance(spender.$, addedValue);
        }, callbacks, `${await this.getPrimaryAccount()} increases ${this.contractName} allowance of ${spender.$} with another ${addedValue}`);
    }
    async decreaseAllowance(spender, removedValue, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.decreaseAllowance(spender.$, removedValue);
        }, callbacks, `${await this.getPrimaryAccount()} descreases ${this.contractName} allowance of ${spender.$} with ${removedValue}`);
    }
}
exports.default = ERC20;
