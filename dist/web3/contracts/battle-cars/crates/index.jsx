"use strict";
// created from 'create-ts-index'
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.S1Crate = void 0;
var S1Crate_1 = require("./S1Crate");
Object.defineProperty(exports, "S1Crate", { enumerable: true, get: function () { return __importDefault(S1Crate_1).default; } });
