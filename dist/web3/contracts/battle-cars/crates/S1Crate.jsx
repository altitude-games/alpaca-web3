"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Crate_1 = __importDefault(require("../Crate"));
class S1Crate extends Crate_1.default {
    constructor(contractName, networkConfig, overrides) {
        super(contractName, networkConfig, overrides);
    }
}
exports.default = S1Crate;
