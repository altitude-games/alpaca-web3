/// <reference types="node" />
import EthereumAddress from "../../EthereumAddress";
import ERC721 from "../../contracts/ERC721";
import ChainNetConfig from '../../types/ChainNetConfig';
import TokenId from "../../types/TokenId";
import { ChainConfigOverride } from "../../utils/ContractFactory";
/**
 * Class contract spec for Battle Racers Part specifics
 */
export default class CarPart extends ERC721 {
    constructor(contractName: string, networkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    transferTokenFrom(from: EthereumAddress, to: EthereumAddress, tokenId: TokenId, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    transferFrom(from: EthereumAddress, to: EthereumAddress, tokenId: TokenId, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    mintPart(owner: EthereumAddress, tokenId: TokenId, tokenURI: string, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    mintPartSigned(owner: EthereumAddress, tokenId: TokenId, tokenURI: string, signature: Buffer, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    enable(callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    disable(callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
}
