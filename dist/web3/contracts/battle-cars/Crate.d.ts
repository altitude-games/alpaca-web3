import EthereumAddress from '../../EthereumAddress';
import ChainNetConfig from '../../types/ChainNetConfig';
import ERC20 from "../ERC20";
import { ChainConfigOverride } from "../../utils/ContractFactory";
export default class Crate extends ERC20 {
    private __crateName;
    constructor(contractName: string, networkConfig: ChainNetConfig, overrides?: ChainConfigOverride);
    get contractName(): string;
    purchase(amount: number, referrer: EthereumAddress, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    purchaseFor(buyer: EthereumAddress, amount: number, referrer: EthereumAddress, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    price(): Promise<number>;
}
