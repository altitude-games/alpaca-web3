"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ERC20_1 = __importDefault(require("../ERC20"));
const ContractFactory_1 = __importDefault(require("../../utils/ContractFactory"));
class Crate extends ERC20_1.default {
    constructor(contractName, networkConfig, overrides) {
        const dappObjects = ContractFactory_1.default(contractName, networkConfig, overrides);
        super({ ...dappObjects, contractName, networkConfig, addresses: [], overrides });
        this.__crateName = contractName;
    }
    get contractName() {
        return this.__crateName;
    }
    async purchase(amount, referrer, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.purchase(amount, referrer.$);
        }, callbacks, `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName}, referred by ${referrer.$}`);
    }
    async purchaseFor(buyer, amount, referrer, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.purchaseFor(buyer.$, amount, referrer.$);
        }, callbacks, `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName} for ${buyer.$}, referred by ${referrer.$}`);
    }
    async price() {
        if (!this._cache.cratePrice) {
            this._cache.cratePrice = Number(await this._performContractCall((contract) => {
                return contract.methods.price();
            }, `getting ${this.contractName}'s price`));
        }
        return this._cache.cratePrice;
    }
}
exports.default = Crate;
