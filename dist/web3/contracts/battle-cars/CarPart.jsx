"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ERC721_1 = __importDefault(require("../../contracts/ERC721"));
const ContractFactory_1 = __importDefault(require("../../utils/ContractFactory"));
/**
 * Class contract spec for Battle Racers Part specifics
 */
class CarPart extends ERC721_1.default {
    constructor(contractName, networkConfig, overrides) {
        const dappObjects = ContractFactory_1.default(contractName, networkConfig, overrides);
        super({ ...dappObjects, contractName, networkConfig, addresses: [] });
    }
    async transferTokenFrom(from, to, tokenId, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.transferTokenFrom(from.$, to.$, tokenId);
        }, callbacks, `Transfer ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`);
    }
    async transferFrom(from, to, tokenId, callbacks) {
        return this.transferTokenFrom(from, to, tokenId, callbacks);
    }
    async mintPart(owner, tokenId, tokenURI, callbacks) {
        return await this._performContractSend((contract) => {
            return contract.methods.mintPart(owner.$, tokenId, tokenURI);
        }, callbacks, `${await this.getPrimaryAccount()} mints ${this.contractName} ${tokenId} ${tokenURI} to account ${owner.$}`);
    }
    async mintPartSigned(owner, tokenId, tokenURI, signature, callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.mintPartSigned(owner.$, tokenId, tokenURI, signature);
        }, callbacks, `${await this.getPrimaryAccount()} performs signed mint ${this.contractName} ${tokenId} ${tokenURI} to account ${owner.$}`);
    }
    async enable(callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.enable();
        }, callbacks, `${await this.getPrimaryAccount()} enables contracts ${this.contractName} ${this.address}`);
    }
    async disable(callbacks) {
        return this._performContractSend((contract) => {
            return contract.methods.disable();
        }, callbacks, `${await this.getPrimaryAccount()} disabled contracts ${this.contractName} ${this.address}`);
    }
}
exports.default = CarPart;
