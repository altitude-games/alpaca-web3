import EthereumAddress from '../EthereumAddress';
import IERC20 from '../types/IERC20';
import DAppArtifacts from '../types/DAppArtifacts';
import Wallet from '../Wallet';
import ContractBase from "./ContractBase";
export default class ERC20 extends ContractBase implements IERC20 {
    constructor(artifacts: DAppArtifacts);
    decimals(): Promise<any>;
    balanceOf(wallet: Wallet): Promise<number>;
    transfer(recipient: EthereumAddress, amount: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    allowance(owner: EthereumAddress, spender: EthereumAddress): Promise<number>;
    approve(spender: EthereumAddress, amount: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    mint(recipient: EthereumAddress, amount: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    transferFrom(sender: EthereumAddress, recipient: EthereumAddress, amount: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    increaseAllowance(spender: EthereumAddress, addedValue: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
    decreaseAllowance(spender: EthereumAddress, removedValue: number, callbacks?: {
        [event: string]: Function;
    }): Promise<any>;
}
