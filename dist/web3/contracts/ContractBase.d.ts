import Web3 from "web3";
import Bunyan from "bunyan";
import { Contract, Filter } from 'web3-eth-contract';
import DAppArtifacts from "../types/DAppArtifacts";
import { ChainConfigOverride } from "../utils";
import { ChainNetConfig } from "../types";
import { EthereumAddress } from "..";
export default class ContractBase {
    protected _artifacts: DAppArtifacts;
    protected _logger: Bunyan;
    protected _cache: any;
    constructor(artifacts: DAppArtifacts);
    tokenName(): Promise<any>;
    tokenSymbol(): Promise<any>;
    get address(): typeof EthereumAddress;
    get networkConfig(): ChainNetConfig;
    get contractName(): string;
    get contract(): Contract;
    get web3(): Web3;
    get provider(): any;
    get addresses(): Array<EthereumAddress>;
    get migrationHash(): string;
    get networkOverrides(): ChainConfigOverride | undefined;
    set networkOverrides(overrides: ChainConfigOverride | undefined);
    /**
     * Retrieves events from past blocks
     *
     * @param eventName name of events e.g. Transfer
     * @param startBlock optional, if not specified will auto detect starting from migration block of contract and succeeding calls will just increment from last block obtained
     * @param endBlock optional, will be detected based on latest block or increment of ChainNetConfig.networkBlockSteps - whichever is lower
     * @param filter optional filter as per web3 documentation
     */
    getPastEvents(eventName: string, startBlock?: number, endBlock?: number, filter?: Filter): Promise<{
        events: import("web3-eth-contract").EventData[];
        fromBlock: number;
        toBlock: number;
    }>;
    private __initializeWallet;
    get accounts(): Array<EthereumAddress>;
    set accounts(accounts: Array<EthereumAddress>);
    getPrimaryAccount(): Promise<EthereumAddress>;
    protected _currentGasPrice(): Promise<string>;
    /**
     * Send transaction wrapper with pre-emptive checks for gas expenditure and gas price to avoid wasted calls and losing ethereum.
     * Adding the functionality of auto retry via endpoint rotations.
     *
     * @param contractMethodGetter a function that accepts contract object to return the method of the contract to send
     * @param functionDesc optional message to describe this call
     */
    protected _performContractSend(contractMethodGetter: (contract: Contract) => any, callbacks?: {
        [events: string]: Function;
    }, functionDesc?: string): Promise<(any | null)>;
    /**
     * Call wrapper that provides auto retry with endpoint rotations.
     *
     * @param contractMethodGetter a function that accepts contract object to return the method of the contract to call
     * @param functionDesc optional message to describe this call
     */
    protected _performContractCall<T>(contractMethodGetter: (contract: Contract) => any, functionDesc?: string): Promise<(T | null)>;
    cleanUp(): void;
}
