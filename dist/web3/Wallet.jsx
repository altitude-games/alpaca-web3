"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EthereumAddress_1 = __importDefault(require("./EthereumAddress"));
class Wallet extends EthereumAddress_1.default {
    constructor(walletAddr, ownerName = "") {
        super(walletAddr);
        this.__walletName = "";
        this.toString = () => `${super.toString()}${this.__walletName === '' ? '' : ` <${this.__walletName}>`}`;
        this.valueOf = () => `${super.toString()}`;
        this.__walletName = ownerName.trim();
    }
}
exports.default = Wallet;
