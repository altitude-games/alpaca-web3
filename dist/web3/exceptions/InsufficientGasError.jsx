"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InsufficientGasError extends Error {
    constructor(gasAlloted, gasRequired) {
        super(`Insufficient Gas - Limit: ${gasAlloted} Required: ${gasRequired}`);
    }
}
exports.default = InsufficientGasError;
