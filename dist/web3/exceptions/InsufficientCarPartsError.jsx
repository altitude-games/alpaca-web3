"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InsufficientCarPartsError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.default = InsufficientCarPartsError;
