export default class InsufficientCarPartsError extends Error {
    constructor(message: string);
}
