"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LowGasPriceError extends Error {
    constructor(gasPriceOffer, currentNetworkGasPrice) {
        super(`Low Gas Price Offer - Configured: ${gasPriceOffer} Current: ${currentNetworkGasPrice}`);
    }
}
exports.default = LowGasPriceError;
