export default class LowGasPriceError extends Error {
    constructor(gasPriceOffer: string, currentNetworkGasPrice: string);
}
