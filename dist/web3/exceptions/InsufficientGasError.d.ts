export default class InsufficientGasError extends Error {
    constructor(gasAlloted: number, gasRequired: number);
}
