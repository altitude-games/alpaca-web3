"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = __importDefault(require("web3"));
exports.default = (chainAddr) => {
    const ethAddrPattern = /^0x[a-fA-F0-9]{40}$/g;
    if (ethAddrPattern.test(chainAddr)) {
        return web3_1.default.utils.toChecksumAddress(chainAddr);
    }
    else {
        throw new Error(`Invalid Ethereum Address -- ${chainAddr}`);
    }
};
