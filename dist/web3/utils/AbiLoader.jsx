"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const bunyan_1 = __importDefault(require("bunyan"));
/**
 * Loads contract with optional network address information override (if it exists).
 */
const abiCache = {};
const logger = bunyan_1.default.createLogger({ name: 'AbiLoader', level: process.env.NODE_ENV === 'development' ? 'debug' : 'info' });
const loadContractJsonAbi = (contract, networkId) => {
    const cacheKey = `${contract}/${networkId}`;
    if (!abiCache[cacheKey]) {
        logger.debug(`loadNetworkContract  ${contract}, ${networkId}`);
        const contractJson = loadJsonFile(contract);
        if (contractJson) {
            const netDataJson = loadJsonFile(`networks/${contract}/${networkId}`);
            if (netDataJson) {
                contractJson.networks[networkId.toString()] = netDataJson;
            }
            abiCache[cacheKey] = contractJson;
        }
        else {
            throw new Error(`Unable to load contract ${contract}`);
        }
    }
    return abiCache[cacheKey];
};
const loadJsonFile = (jsonFileName) => {
    let jsonContents = null;
    const locations = [
        path_1.default.resolve(process.cwd(), path_1.default.join('.', 'contracts', `${jsonFileName}.json`)),
        path_1.default.resolve(process.cwd(), path_1.default.join('.', 'src', 'contracts', `${jsonFileName}.json`)),
    ];
    if (process.env.CONTRACT_PATH) {
        locations.unshift(path_1.default.resolve(process.env.CONTRACT_PATH, `${jsonFileName}.json`));
        locations.unshift(path_1.default.resolve(process.cwd(), path_1.default.join(process.env.CONTRACT_PATH, `${jsonFileName}.json`)));
    }
    logger.info(`Contract load paths:\n${locations.join("\n")}`);
    for (let i = 0; i < locations.length; i++) {
        const jsonFilePath = locations[i];
        logger.debug(`Trying to loading ${jsonFileName} from ${jsonFilePath}`);
        if (fs_1.default.existsSync(jsonFilePath)) {
            logger.debug(`\tFound ${jsonFileName} on ${jsonFilePath}`);
            const contents = fs_1.default.readFileSync(jsonFilePath, 'utf8');
            try {
                jsonContents = JSON.parse(contents);
                logger.debug(`\t${jsonFileName} successfully parsed.`);
            }
            catch (err) { }
        }
    }
    return jsonContents;
};
exports.default = loadContractJsonAbi;
