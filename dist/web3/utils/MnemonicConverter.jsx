"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = __importDefault(require("web3"));
const bunyan_1 = __importDefault(require("bunyan"));
const ethereumjs_wallet_1 = require("ethereumjs-wallet");
const bip39 = require("bip39");
const logger = bunyan_1.default.createLogger({
    name: `MnemonicConverter`,
    level: process.env.NODE_ENV === "development" ? bunyan_1.default.DEBUG : bunyan_1.default.INFO,
});
exports.default = async (mnemonic, addressIndex = 0, numAddresses = 1, derivationpath = "m/44'/60'/0'/0/") => {
    //logger.debug('Mnemonic: ', mnemonic, addressIndex, numAddresses, derivationpath);
    const addresses = [];
    const seed = await bip39.mnemonicToSeed(mnemonic);
    const hdwallet = await ethereumjs_wallet_1.hdkey.fromMasterSeed(seed);
    //logger.debug(`seed`, seed);
    //logger.debug(`hdwallet`, hdwallet);
    for (let i = addressIndex; i < addressIndex + numAddresses; i++) {
        const wallet = hdwallet.derivePath(derivationpath + i).getWallet();
        const addr = '0x' + wallet.getAddress().toString('hex');
        //logger.debug(`Addr -- `, addr);
        addresses.push(web3_1.default.utils.toChecksumAddress(addr));
    }
    return addresses;
};
