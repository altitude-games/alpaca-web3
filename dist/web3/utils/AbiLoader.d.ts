declare const loadContractJsonAbi: (contract: string, networkId: number | string) => any;
export default loadContractJsonAbi;
