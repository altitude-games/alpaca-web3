"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bunyan_1 = __importDefault(require("bunyan"));
const AbiLoader_1 = __importDefault(require("./AbiLoader"));
const Web3Factory_1 = __importStar(require("./Web3Factory"));
const abiCache = {};
const logger = bunyan_1.default.createLogger({
    name: `ContractFactory`,
    level: process.env.NODE_ENV === "development" ? bunyan_1.default.DEBUG : bunyan_1.default.INFO,
});
exports.default = (contractName, networkConfig, overrides = {}) => {
    const abiCacheKey = `${contractName}/${networkConfig.networkId}`;
    if (!abiCache[abiCacheKey]) {
        abiCache[abiCacheKey] = AbiLoader_1.default(contractName, networkConfig.networkId);
    }
    const providerFactory = Web3Factory_1.PROVIDER_FACTORY_MAPPING[(overrides.useProvider || networkConfig.useProvider)];
    const web3 = Web3Factory_1.default(networkConfig, providerFactory);
    const options = { ...networkConfig.options, ...overrides.options };
    //logger.debug(`abiCache[${abiCacheKey}].networks[${networkConfig.networkId}]`, abiCache[abiCacheKey].networks[networkConfig.networkId]);
    if (!abiCache[abiCacheKey].networks[networkConfig.networkId]) {
        throw new Error(`${contractName} is not deployed to network ${networkConfig.networkId}`);
    }
    return {
        contract: new web3.eth.Contract(abiCache[abiCacheKey].abi, abiCache[abiCacheKey].networks[networkConfig.networkId].address, options),
        web3
    };
};
