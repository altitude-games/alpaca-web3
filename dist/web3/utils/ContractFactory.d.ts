import { WebsocketProviderOptions, HttpProviderOptions } from 'web3-core-helpers';
import ChainNetConfig from "../types/ChainNetConfig";
export interface ChainConfigOverride {
    httpOptions?: HttpProviderOptions;
    wsOptions?: WebsocketProviderOptions;
    useProvider?: 'http' | 'ws' | 'ipc' | 'hdwallet' | 'wsHDwallet';
    options?: {
        gas?: number;
        gasPrice?: string;
        from?: string;
    };
}
declare const _default: (contractName: string, networkConfig: ChainNetConfig, overrides?: ChainConfigOverride) => {
    contract: import("web3-eth-contract").Contract;
    web3: import("web3").default;
};
export default _default;
