"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ethereumjs_wallet_1 = __importDefault(require("ethereumjs-wallet"));
const EthUtil = __importStar(require("ethereumjs-util"));
/** code stolen from Truffle HDWallet Provider */
exports.default = async (privateKeys, addressIndex = 0, numAddresses = 1) => {
    const addresses = [];
    // crank the addresses out
    for (let i = addressIndex; i < privateKeys.length && addresses.length <= numAddresses; i++) {
        const privateKey = Buffer.from(privateKeys[i].replace("0x", ""), "hex");
        if (EthUtil.isValidPrivate(privateKey)) {
            const wallet = ethereumjs_wallet_1.default.fromPrivateKey(privateKey);
            const address = wallet.getAddressString();
            addresses.push(address);
        }
    }
    return addresses;
};
