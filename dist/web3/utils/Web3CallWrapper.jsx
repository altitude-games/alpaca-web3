"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const web3_1 = __importDefault(require("web3"));
const bunyan_1 = __importDefault(require("bunyan"));
const _1 = require(".");
/* import { Web3Provider } from "./Web3Factory"; */
const hdwallet_provider_1 = __importDefault(require("@truffle/hdwallet-provider"));
const WebsocketProvider = require('web3-providers-ws');
const HttpProvider = require('web3-providers-http');
const IpcProvider = require('web3-providers-ipc');
const logger = bunyan_1.default.createLogger({
    name: `web3CallWrapper`,
    level: process.env.NODE_ENV === "development" ? bunyan_1.default.DEBUG : bunyan_1.default.INFO,
});
async function web3CallWrapper(web3Func, networkConfig, providerFactory, functionDesc) {
    if (functionDesc) {
        logger.info(`[web3CallWrapper] ${functionDesc}`);
    }
    const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);
    let web3CallResult = null;
    for (let i = 0; i < retryAttempts; i++) {
        try {
            const web3 = _1.getWeb3(networkConfig, providerFactory);
            web3CallResult = await web3Func(web3)
                .then(async (result) => {
                return result;
            })
                .catch(async (err) => {
                return null;
            });
            //clean up hdwallet instance on error
            const provider = (web3.givenProvider || web3.currentProvider);
            if (provider instanceof hdwallet_provider_1.default) {
                provider.engine.stop();
            }
            else if (provider instanceof web3_1.default.providers.WebsocketProvider) {
                provider.disconnect(0, 'clean up');
            }
            else if (provider instanceof web3_1.default.providers.IpcProvider) {
                provider.reset();
            }
            break; //break if finished without errors
        }
        catch (err) {
            logger.error(`${!!functionDesc ? functionDesc : " [web3CallWrapper] "} [error] -- ${err}`);
            logger.error(err);
        }
    }
    return web3CallResult;
}
exports.default = web3CallWrapper;
