import Web3 from "web3";
import HDWalletProvider from '@truffle/hdwallet-provider';
import ChainNetConfig, { HTTP_PROVIDER, WS_PROVIDER, IPC_PROVIDER, HD_WALLET_PROVIDER, HD_WALLET_PROVIDER_WS } from "../types/ChainNetConfig";
export declare const web3ProviderFactory: (config: ChainNetConfig, providerType?: ("http" | "ws" | "ipc")) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
export declare const httpProviderFactory: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
export declare const wsProviderFactory: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
export declare const ipcProviderFactory: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
export declare const hdWalletProviderFactory: (config: ChainNetConfig, providerType?: ("http" | "ws")) => HDWalletProvider;
export declare const hdWalletHttpProviderFactory: (config: ChainNetConfig) => HDWalletProvider;
export declare const hdWalletWsProviderFactory: (config: ChainNetConfig) => HDWalletProvider;
export declare const PROVIDER_FACTORY_MAPPING: {
    http: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
    ws: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
    ipc: (config: ChainNetConfig) => import("web3-core").IpcProvider | import("web3-core").WebsocketProvider | import("web3-core").HttpProvider;
    hdwallet: (config: ChainNetConfig) => HDWalletProvider;
    wsHDwallet: (config: ChainNetConfig) => HDWalletProvider;
};
declare const _default: (networkConfig: ChainNetConfig, providerFactory?: (config: ChainNetConfig) => any) => Web3;
/**
 * creates web3 instance with provided provider or use connection balancing using multiple
 * endpoint URLS
 */
export default _default;
