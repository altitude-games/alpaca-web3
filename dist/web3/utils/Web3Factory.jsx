"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PROVIDER_FACTORY_MAPPING = exports.hdWalletWsProviderFactory = exports.hdWalletHttpProviderFactory = exports.hdWalletProviderFactory = exports.ipcProviderFactory = exports.wsProviderFactory = exports.httpProviderFactory = exports.web3ProviderFactory = void 0;
const web3_1 = __importDefault(require("web3"));
const bunyan_1 = __importDefault(require("bunyan"));
const hdwallet_provider_1 = __importDefault(require("@truffle/hdwallet-provider"));
const ChainNetConfig_1 = require("../types/ChainNetConfig");
/* const WebsocketProvider = require('web3-providers-ws');
const HttpProvider = require('web3-providers-http');
const IpcProvider = require('web3-providers-ipc');

export type Web3Provider = Web3.providers.HttpProvider | Web3.providers.WebsocketProvider | Web3.providers.IpcProvider | HDWalletProvider; */
const logger = bunyan_1.default.createLogger({
    name: `Web3Factory`,
    level: process.env.NODE_ENV === "development" ? bunyan_1.default.DEBUG : bunyan_1.default.INFO,
});
const web3CacheProviderIndex = {};
const baseWsOptions = {
    timeout: 30000,
    // Useful for credentialed urls, e.g: ws://username:password@localhost:8546
    /* headers: {
      authorization: 'Basic username:password'
    }, */
    clientConfig: {
        // Useful if requests are large
        maxReceivedFrameSize: 100000000,
        maxReceivedMessageSize: 100000000,
        // Useful to keep a connection alive
        keepalive: true,
        keepaliveInterval: 60000 // ms
    },
    // Enable auto reconnection
    reconnect: {
        auto: true,
        delay: 5000,
        maxAttempts: 5,
        onTimeout: false
    }
};
const web3ProviderFactory = (config, providerType = ChainNetConfig_1.HTTP_PROVIDER) => {
    //logger.debug(config, providerType);
    const web3CacheKey = `${providerType}/${config.networkId}`;
    const wsUrls = config.wsUrl || [];
    const ipcUrl = config.ipcUrl || "";
    if (providerType === ChainNetConfig_1.WS_PROVIDER && wsUrls.length == 0) {
        throw (`Cannot use websocket providers when no WS Provider URL given.`);
    }
    if (providerType === ChainNetConfig_1.IPC_PROVIDER && ipcUrl === "") {
        throw (`Cannot use IPC provider when no IPC Provider given.`);
    }
    let providerIndex = !web3CacheProviderIndex[web3CacheKey] ? 0 : web3CacheProviderIndex[web3CacheKey];
    let providerUrl = "";
    if (providerType !== ChainNetConfig_1.IPC_PROVIDER) {
        if (providerIndex >= (providerType === ChainNetConfig_1.HTTP_PROVIDER ? config.httpUrl : wsUrls).length) {
            providerIndex = 0;
        }
        providerUrl = (providerType === ChainNetConfig_1.HTTP_PROVIDER ? config.httpUrl : wsUrls)[providerIndex];
    }
    else {
        providerUrl = ipcUrl;
    }
    //save the pointer and increment for next call
    web3CacheProviderIndex[web3CacheKey] = providerIndex + 1;
    logger.debug(`web3CacheProviderIndex[${web3CacheKey}]`, web3CacheProviderIndex[web3CacheKey]);
    logger.debug(`providerUrl = `, providerUrl);
    if (providerType === ChainNetConfig_1.IPC_PROVIDER) {
        const net = require('net');
        return new web3_1.default.providers.IpcProvider(providerUrl, net);
    }
    else if (providerType === ChainNetConfig_1.WS_PROVIDER) {
        return new web3_1.default.providers.WebsocketProvider(providerUrl, config.wsOptions || baseWsOptions);
    }
    else {
        return new web3_1.default.providers.HttpProvider(providerUrl, config.httpOptions);
    }
};
exports.web3ProviderFactory = web3ProviderFactory;
const httpProviderFactory = (config) => {
    return exports.web3ProviderFactory(config);
};
exports.httpProviderFactory = httpProviderFactory;
const wsProviderFactory = (config) => {
    return exports.web3ProviderFactory(config, ChainNetConfig_1.WS_PROVIDER);
};
exports.wsProviderFactory = wsProviderFactory;
const ipcProviderFactory = (config) => {
    return exports.web3ProviderFactory(config, ChainNetConfig_1.IPC_PROVIDER);
};
exports.ipcProviderFactory = ipcProviderFactory;
const hdWalletProviderFactory = (config, providerType = "http") => {
    const web3CacheKey = `HDWallet/${providerType}/${config.networkId}`;
    const wsUrls = config.wsUrl || [];
    if (providerType === ChainNetConfig_1.WS_PROVIDER && wsUrls.length == 0) {
        throw (`Cannot use websocket providers when no WS Provider URL given.`);
    }
    let providerIndex = !web3CacheProviderIndex[web3CacheKey] ? 0 : web3CacheProviderIndex[web3CacheKey];
    if (providerIndex >= (providerType === ChainNetConfig_1.HTTP_PROVIDER ? config.httpUrl : wsUrls).length) {
        providerIndex = 0;
    }
    web3CacheProviderIndex[web3CacheKey] = providerIndex + 1;
    const providerUrl = (providerType === ChainNetConfig_1.HTTP_PROVIDER ? config.httpUrl : wsUrls)[providerIndex];
    const providerOrUrl = providerType === ChainNetConfig_1.HTTP_PROVIDER ? providerUrl : new web3_1.default.providers.WebsocketProvider(providerUrl, config.wsOptions || baseWsOptions);
    return new hdwallet_provider_1.default({
        ...config.walletOptions,
        providerOrUrl
    });
};
exports.hdWalletProviderFactory = hdWalletProviderFactory;
const hdWalletHttpProviderFactory = (config) => {
    return exports.hdWalletProviderFactory(config, ChainNetConfig_1.HTTP_PROVIDER);
};
exports.hdWalletHttpProviderFactory = hdWalletHttpProviderFactory;
const hdWalletWsProviderFactory = (config) => {
    return exports.hdWalletProviderFactory(config, ChainNetConfig_1.WS_PROVIDER);
};
exports.hdWalletWsProviderFactory = hdWalletWsProviderFactory;
exports.PROVIDER_FACTORY_MAPPING = {
    [ChainNetConfig_1.HTTP_PROVIDER]: exports.httpProviderFactory,
    [ChainNetConfig_1.WS_PROVIDER]: exports.wsProviderFactory,
    [ChainNetConfig_1.IPC_PROVIDER]: exports.ipcProviderFactory,
    [ChainNetConfig_1.HD_WALLET_PROVIDER]: exports.hdWalletHttpProviderFactory,
    [ChainNetConfig_1.HD_WALLET_PROVIDER_WS]: exports.hdWalletWsProviderFactory
};
/**
 * creates web3 instance with provided provider or use connection balancing using multiple
 * endpoint URLS
 */
exports.default = (networkConfig, providerFactory = exports.httpProviderFactory) => {
    return new web3_1.default(providerFactory(networkConfig));
};
