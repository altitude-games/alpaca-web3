declare const _default: (mnemonic: string, addressIndex?: number, numAddresses?: number, derivationpath?: string) => Promise<string[]>;
export default _default;
