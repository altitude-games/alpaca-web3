import Web3 from "web3";
import { ChainNetConfig } from "../types";
declare function web3CallWrapper<T>(web3Func: (web3: Web3) => Promise<T>, networkConfig: ChainNetConfig, providerFactory?: (config: ChainNetConfig) => any, functionDesc?: string): Promise<T | null>;
export default web3CallWrapper;
