declare const _default: (privateKeys: string[], addressIndex?: number, numAddresses?: number) => Promise<string[]>;
/** code stolen from Truffle HDWallet Provider */
export default _default;
