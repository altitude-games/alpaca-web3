export default class EthereumAddress {
    protected _wallet: string;
    constructor(walletAddr: string);
    toString(): string;
    valueOf(): string;
    get value(): string;
    get $(): string;
    is(compareTo: string | EthereumAddress): boolean;
    equals(compareTo: string | EthereumAddress): boolean;
}
