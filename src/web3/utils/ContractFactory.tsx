import Bunyan from "bunyan";
import { ContractOptions } from 'web3-eth-contract';
import { WebsocketProviderOptions, HttpProviderOptions } from 'web3-core-helpers';
import ChainNetConfig from "../types/ChainNetConfig";
import loadContractJsonAbi from "./AbiLoader";

import getWeb3, { 
  PROVIDER_FACTORY_MAPPING,
} from "./Web3Factory";

const abiCache:{
  [key:string]: any;
} = {};

const logger = Bunyan.createLogger({
  name: `ContractFactory`,
  level: process.env.NODE_ENV === "development" ? Bunyan.DEBUG : Bunyan.INFO,
});

export interface ChainConfigOverride {
  httpOptions?:HttpProviderOptions;
  wsOptions?:WebsocketProviderOptions;
  useProvider?: 'http' | 'ws' | 'ipc' | 'hdwallet' | 'wsHDwallet';
  options?: {
    gas?: number;
    gasPrice?:string;
    from?:string;
  }
}

export default (contractName:string, networkConfig:ChainNetConfig, overrides:ChainConfigOverride = {}) => {
  const abiCacheKey = `${contractName}/${networkConfig.networkId}`;
  if(!abiCache[abiCacheKey]){
    abiCache[abiCacheKey] = loadContractJsonAbi(contractName, networkConfig.networkId);
  }
  const providerFactory = PROVIDER_FACTORY_MAPPING[(overrides.useProvider || networkConfig.useProvider)];

  const web3 = getWeb3(networkConfig, providerFactory);
  const options:ContractOptions = { ...networkConfig.options, ...overrides.options };

  //logger.debug(`abiCache[${abiCacheKey}].networks[${networkConfig.networkId}]`, abiCache[abiCacheKey].networks[networkConfig.networkId]);
  if(!abiCache[abiCacheKey].networks[networkConfig.networkId]){
    throw new Error(`${contractName} is not deployed to network ${networkConfig.networkId}`);
  }

  return {
    contract: new web3.eth.Contract(abiCache[abiCacheKey].abi, abiCache[abiCacheKey].networks[networkConfig.networkId].address, options),
    web3
  };
}