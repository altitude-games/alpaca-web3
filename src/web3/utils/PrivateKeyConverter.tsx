import ethJSWallet from "ethereumjs-wallet";
import * as EthUtil from "ethereumjs-util";

/** code stolen from Truffle HDWallet Provider */
export default async(privateKeys: string[], addressIndex:number = 0, numAddresses:number = 1) => {
  const addresses = [];

  // crank the addresses out
  for (let i = addressIndex; i < privateKeys.length && addresses.length <= numAddresses; i++) {
    const privateKey = Buffer.from(privateKeys[i].replace("0x", ""), "hex");
    if (EthUtil.isValidPrivate(privateKey)) {
      const wallet = ethJSWallet.fromPrivateKey(privateKey);
      const address = wallet.getAddressString();
      
      addresses.push(address);
    }
  }

  return addresses;
};