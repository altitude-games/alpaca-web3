import Web3 from "web3";
import Bunyan from "bunyan";
import { ChainNetConfig } from "../types";
import { getWeb3 } from ".";
/* import { Web3Provider } from "./Web3Factory"; */

import HDWalletProvider from "@truffle/hdwallet-provider";

const WebsocketProvider = require('web3-providers-ws');
const HttpProvider = require('web3-providers-http');
const IpcProvider = require('web3-providers-ipc');

const logger = Bunyan.createLogger({
  name: `web3CallWrapper`,
  level: process.env.NODE_ENV === "development" ? Bunyan.DEBUG : Bunyan.INFO,
});

async function web3CallWrapper<T>(
  web3Func: (web3: Web3) => Promise<T>,
  networkConfig: ChainNetConfig,
  providerFactory?: (config: ChainNetConfig) => any,
  functionDesc?: string
): Promise<T | null> {

  if (functionDesc) {
    logger.info(`[web3CallWrapper] ${functionDesc}`);
  }

  const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);

  let web3CallResult: T | null = null;

  for (let i: number = 0; i < retryAttempts; i++) {
    try {
      const web3 = getWeb3(networkConfig, providerFactory);

      web3CallResult = await web3Func(web3)
        .then(async (result) => {
          return result;
        })
        .catch(async (err) => {
          return null;
        });

      //clean up hdwallet instance on error
      const provider = (web3.givenProvider || web3.currentProvider);
      if (provider instanceof HDWalletProvider) {
        provider.engine.stop();
      } else if(provider instanceof Web3.providers.WebsocketProvider){
        provider.disconnect(0, 'clean up');
      } else  if(provider instanceof Web3.providers.IpcProvider){
        provider.reset();
      }

      break; //break if finished without errors
      
    } catch (err) {
      logger.error(
        `${
          !!functionDesc ? functionDesc : " [web3CallWrapper] "
        } [error] -- ${err}`
      );
      logger.error(err);
    }
  }

  return web3CallResult;
}

export default web3CallWrapper;
