import path from 'path';
import fs from 'fs';
import bunyan from 'bunyan';

/**
 * Loads contract with optional network address information override (if it exists).
 */
const abiCache:{
  [key:string]:any;
} = {};

const logger = bunyan.createLogger({ name: 'AbiLoader', level: process.env.NODE_ENV === 'development' ? 'debug' : 'info' });

const loadContractJsonAbi = (contract:string, networkId:number | string):any => {
  const cacheKey = `${contract}/${networkId}`;

  if(!abiCache[cacheKey]){
    logger.debug(`loadNetworkContract  ${contract}, ${networkId}`);

    const contractJson = loadJsonFile( contract );

    if(contractJson){
      const netDataJson = loadJsonFile(`networks/${contract}/${networkId}`);

      if(netDataJson){
        contractJson.networks[networkId.toString()] = netDataJson;
      }

      abiCache[cacheKey] = contractJson;
    } else {
      throw new Error(`Unable to load contract ${contract}`);
    }
  }

  return abiCache[cacheKey];
}

const loadJsonFile = (jsonFileName:string):any => {
  let jsonContents:any = null;
  const locations = [
    path.resolve(process.cwd(), path.join('.', 'contracts', `${jsonFileName}.json`)),
    path.resolve(process.cwd(), path.join('.', 'src', 'contracts', `${jsonFileName}.json`)),
  ];

  if(process.env.CONTRACT_PATH){
    locations.unshift(path.resolve(process.env.CONTRACT_PATH, `${jsonFileName}.json`));
    locations.unshift(path.resolve(process.cwd(), path.join(process.env.CONTRACT_PATH, `${jsonFileName}.json`)));
  }

  logger.info(`Contract load paths:\n${locations.join("\n")}`);

  for(let i:number = 0; i < locations.length; i++){
    const jsonFilePath = locations[i];

    logger.debug(`Trying to loading ${jsonFileName} from ${jsonFilePath}`);

    if(fs.existsSync(jsonFilePath)){
      logger.debug(`\tFound ${jsonFileName} on ${jsonFilePath}`);
      const contents = fs.readFileSync(jsonFilePath, 'utf8');
  
      try {
        jsonContents = JSON.parse(contents);
        logger.debug(`\t${jsonFileName} successfully parsed.`);
      } catch(err) {}
    }
  }
  
  return jsonContents;
}

export default loadContractJsonAbi;