import { ChainNetConfig } from "../../../types";
import Crate from "../Crate"
import { ChainConfigOverride } from '../../../utils';

export default class S1Crate extends Crate {
  constructor(contractName:string, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, networkConfig, overrides);
  }
}