import { Contract } from 'web3-eth-contract';

import EthereumAddress from '../../EthereumAddress';
import ChainNetConfig from '../../types/ChainNetConfig';
import ERC20 from "../ERC20";

import getContract, { ChainConfigOverride } from "../../utils/ContractFactory";

export default class Crate extends ERC20 {
  private __crateName:string;
  
  constructor(contractName:string, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    const dappObjects = getContract(contractName, networkConfig, overrides);
    super({ ...dappObjects, contractName, networkConfig, addresses: [], overrides });

    this.__crateName = contractName;
  }

  get contractName():string {
    return this.__crateName;
  }

  async purchase(amount:number, referrer:EthereumAddress, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.purchase(amount, referrer.$);
      },
      callbacks,
      `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName}, referred by ${referrer.$}`
    );
  }

  async purchaseFor(buyer:EthereumAddress, amount:number, referrer:EthereumAddress, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.purchaseFor(buyer.$, amount, referrer.$);
      },
      callbacks,
      `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName} for ${buyer.$}, referred by ${referrer.$}`
    );
  }

  async price():Promise<number>{
    if(!this._cache.cratePrice){
      this._cache.cratePrice = Number(await this._performContractCall(
        (contract:Contract) => {
          return contract.methods.price();
        },
        `getting ${this.contractName}'s price`
      ))
    }

    return this._cache.cratePrice;
  }
}