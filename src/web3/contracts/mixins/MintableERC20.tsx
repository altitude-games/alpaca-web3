import ERC20 from "../ERC20";
import EthereumAddress from "../../EthereumAddress";
import IMintableERC20 from "../../types/IMintableERC20";

type GConstructor<T = {}> = new (...args: any[]) => T;
type ERC20Type = GConstructor<ERC20>;

export default function <TBase extends ERC20Type>(Base: TBase) {
  return class MintableERC20 extends Base implements IMintableERC20 {
    async mint(recipient:EthereumAddress, amount:number){
      await this.contract.methods.mint(recipient.$, amount).send();
    }
  };
}