// created from 'create-ts-index'

export const ADDRESS_0 = "0x0000000000000000000000000000000000000000";

export { default as IBurnableERC20 } from './IBurnableERC20';
export { default as IERC20 } from './IERC20';
export { default as IERC721 } from './IERC721';
export { default as IMintableERC20 } from './IMintableERC20';
export { default as IOwnable } from './IOwnable';
export { default as IToken } from './IToken';
export { default as DAppArtifacts } from './DAppArtifacts';
export { default as ChainNetConfig } from './ChainNetConfig';
export { default as TokenId } from './TokenId';
