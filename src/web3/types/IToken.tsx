import EthereumAddress from "../EthereumAddress";

export default interface IToken {
  tokenName: () => Promise<string>;
  tokenSymbol: () => Promise<string>;
  address: typeof EthereumAddress;
}