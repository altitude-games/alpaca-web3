import EthereumAddress from "../EthereumAddress";
import Wallet from "../Wallet";
import IToken from "./IToken";

export default interface IERC20 extends IToken {
  decimals: () => Promise<number>;
  balanceOf: (owner:Wallet) => Promise<number>;
  transfer: (recipient:EthereumAddress, amount:number) => Promise<any>;
  allowance: (owner:EthereumAddress, spender:EthereumAddress) => Promise<number>;
  approve: (spender:EthereumAddress, amount:number) => Promise<any>;
  transferFrom: (sender:EthereumAddress, recipient:EthereumAddress, amount:number) => Promise<any>;
  increaseAllowance: (spender:EthereumAddress, addedValue:number) => Promise<any>;
  decreaseAllowance: (spender:EthereumAddress, subtractedValue:number) => Promise<any>;
}