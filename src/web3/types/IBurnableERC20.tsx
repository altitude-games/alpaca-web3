import EthereumAddress from "../EthereumAddress";

export default interface IBurnableERC20 {
  burn: (account:EthereumAddress, amount:number) => Promise<void>;
}