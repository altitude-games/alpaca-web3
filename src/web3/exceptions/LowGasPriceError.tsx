export default class LowGasPriceError extends Error {
  constructor(gasPriceOffer:string, currentNetworkGasPrice:string){
    super(`Low Gas Price Offer - Configured: ${gasPriceOffer} Current: ${currentNetworkGasPrice}`);
  }
}