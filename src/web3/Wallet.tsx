import EthereumAddress from "./EthereumAddress";

export default class Wallet extends EthereumAddress {
  private __walletName:string = "";

  constructor(walletAddr:string, ownerName:string = ""){
    super(walletAddr);
    this.__walletName = ownerName.trim();
  }

  toString = () => `${super.toString()}${this.__walletName === '' ? '' : ` <${this.__walletName}>`}`;
  valueOf = () => `${super.toString()}`;
}